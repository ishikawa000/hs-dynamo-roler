{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DyRoler.Main
    ( defaultMain
    ) where

import Control.Exception.Safe (MonadCatch, throwM)
import Control.Exception.Throwable (illegalArgumentException)
import Control.Lens
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Trans.AWS (newEnv, runAWST, within, send, Credentials(..), Region(..))
import Control.Monad.Trans.Either (EitherT(..))
import Control.Monad.Trans.Resource (runResourceT)
import Data.HashMap.Lazy (HashMap)
import Data.Monoid ((<>))
import Data.Text (Text)
import Data.Time.Clock (UTCTime(..), getCurrentTime)
import Network.AWS (AWS)
import Network.AWS.Auth (envAccessKey, envSecretKey)
import Network.AWS.DynamoDB.PutItem (putItem, piItem)
import Network.AWS.DynamoDB.Scan (scan, srsItems)
import Network.AWS.DynamoDB.Types (AttributeValue, attributeValue, avS, avN)
import Network.AWS.Env (Env)
import Safe (headMay)
import System.Environment (getArgs)
import qualified Data.HashMap.Lazy as HM
import qualified Data.Text as T
import qualified Data.Text.IO as TIO


--TODO: Use optparser-generic instead of the better getArgs
--TODO: Write --help
-- |
-- Take command line options.
-- It is either '--help', '--list', or '--add'.
-- (Please see '--help' for more information)
defaultMain :: IO ()
defaultMain = do
  opts <- getArgs
  case headMay opts of
    Nothing       -> fail "No option is specified (Please see --help)"
    Just "--help" -> putStrLn "TOOD (help)"
    Just "--list" -> showTasks
    Just "--add"  -> do
      x <- runEitherT . addTask . T.pack . mconcat $ tail opts
      mapM_ (const $ putStrLn "A task is added !") x
    Just _ -> fail $ "Unknown options are specified (Please see --help): " ++ show opts


-- |
-- A row of `ScanResponse`.
-- This is NOT "rows", this is 'a' row.
type ScanRow = HashMap Text AttributeValue

type NakedScanRow = [(Text, AttributeValue)]


-- |
-- Fetch the rows from DynamoDB's 'DynamoRoler' table.
-- Take IAM Access/Secret key as AWS_ACCESS_KEY_ID/AWS_SECRET_ACCESS_KEY
-- (the environment variable).
--
-- And Show the rows.
showTasks :: IO ()
showTasks = do
  env <- newEnvFromLocal
  runAWSIn Tokyo env $ do
    response <- send $ scan "DynamoRoler"
    mapMOf_ (srsItems . folded . to HM.toList) printRow response
  where
    -- Concatenate `Text` and `Text`, and insert " | " to between these.
    -- But return `Nothing` if `Nothing` is given.
    (<<>>) :: Maybe Text -> Maybe Text -> Maybe Text
    (Just x) <<>> (Just y) = Just $ x <> " | " <> y
    _        <<>> _        = Nothing
    -- Visualize expected columns of a row.
    -- Return `Nothing`
    -- if the unexpected column is detected,
    -- or the expected column doesn't have a value of the expected type.
    columnValue :: (Text, AttributeValue) -> Maybe Text
    columnValue ("ID", attr)           = attr ^. avN
    columnValue ("RegisterDate", attr) = attr ^. avN
    columnValue ("Detail", attr)       = attr ^. avS
    columnValue _                      = Nothing
    -- Pretty print `NakedScanRow`
    printRow :: MonadIO m => NakedScanRow -> m ()
    printRow = liftIO . mapM_ TIO.putStrLn . foldr (<<>>) (Just "") . map columnValue



-- |
-- Put the specified text to DynamoDB's 'DynamoRoler' table.
-- Take IAM Access/Secret key as AWS_ACCESS_KEY_ID/AWS_SECRET_ACCESS_KEY
-- (the environment variable)
addTask :: (MonadIO m, MonadCatch m) => Text -> m ()
addTask ""  = throwM $ illegalArgumentException "An empty task detail was passed"
addTask msg = do
  env <- newEnvFromLocal
  now <- liftIO getCurrentTime
  void . runAWSIn Tokyo env $ do
    newId <- (+ 1) <$> fetchLastId
    send $ putItem "DynamoRoler" & piItem .~ taskRow newId now msg
    updateLastId newId
  where
    -- Make a row of DynamoRoler table
    taskRow :: Int -> UTCTime -> Text -> ScanRow
    taskRow n registerDate detail =
      HM.fromList [ ("ID",           attributeValue & avN .~ Just (T.pack $ show n))
                  , ("RegisterDate", attributeValue & avN .~ Just "0") --TODO: Use registerDate
                  , ("Detail",       attributeValue & avS .~ Just msg)
                  ]
    -- A workaround,
    -- DynamoDB doesn't support auto increment ID.
    -- Throw a fatal exception if DynamoRolerLastID table is not exists,
    -- or another problem is happened.
    fetchLastId :: AWS Int
    fetchLastId = do
      response <- send $ scan "DynamoRolerLastID"
      let maybeLastId = response ^? srsItems . ix 1 . to HM.toList . ix 1 . _2 . avN . _Just . to (read . T.unpack)
      case maybeLastId of
        Nothing -> fail "fetchLastId: cannot get the last id from DynamoRolerLastID table"
        Just n  -> return n
    -- Take a value of new last id
    updateLastId :: Int -> AWS ()
    updateLastId n = do
      let n' = T.pack $ show n
      void . send $ putItem "DynamoRolerLastID" & piItem .~ HM.fromList [("ID", attributeValue & avN .~ Just n')]


-- | Run an `AWSRequest` in some `Region`
runAWSIn :: MonadIO m => Region -> Env -> AWS a -> m a
runAWSIn region env = liftIO . runResourceT . runAWST env . within region


-- | Generate new `Env`, that is loaded with the local envAccessKey/envSecretKey
newEnvFromLocal :: (MonadIO m, MonadCatch m) => m Env
newEnvFromLocal = newEnv $ FromEnv envAccessKey envSecretKey Nothing Nothing
