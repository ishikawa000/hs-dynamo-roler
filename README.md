# hs-dynamo-roler
　DynamoDBにデータを保存するTODO管理ツール。
（DynamoDB via Haskellなアプリケーションのサンプル）


# 用意するテーブル

- 参考ページ
    - [テーブルの作成 - Amazon DynamoDB](http://docs.aws.amazon.com/ja_jp/amazondynamodb/latest/developerguide/SQLtoNoSQL.CreateTable.html)

'DynamoRoler'

| カラム名     | 属性型 |
|--------------|--------|
| ID           | 数値   |
| RegisterDate | 数値   |
| Detail       | 文字列 |

'DynamoRolerLastID'

| カラム名     | 属性型 |
|--------------|--------|
| LastID       | 数値   |
